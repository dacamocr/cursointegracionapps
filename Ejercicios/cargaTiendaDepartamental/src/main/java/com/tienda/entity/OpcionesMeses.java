package com.tienda.entity;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@CsvRecord(separator = ",")
public class OpcionesMeses {

	
	public int getOpcionId() {
		return opcionId;
	}

	public void setOpcionId(int opcionId) {
		this.opcionId = opcionId;
	}

	public int getPrefijoTarjeta() {
		return prefijoTarjeta;
	}

	public void setPrefijoTarjeta(int prefijoTarjeta) {
		this.prefijoTarjeta = prefijoTarjeta;
	}

	public String getInstitutcionBancaria() {
		return institutcionBancaria;
	}

	public void setInstitutcionBancaria(String institutcionBancaria) {
		this.institutcionBancaria = institutcionBancaria;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public int getMonto3Meses() {
		return monto3Meses;
	}

	public void setMonto3Meses(int monto3Meses) {
		this.monto3Meses = monto3Meses;
	}

	public int getMonto6Meses() {
		return monto6Meses;
	}

	public void setMonto6Meses(int monto6Meses) {
		this.monto6Meses = monto6Meses;
	}

	public int getMonto12Meses() {
		return monto12Meses;
	}

	public void setMonto12Meses(int monto12Meses) {
		this.monto12Meses = monto12Meses;
	}

	public String getCreadoEn() {
		return creadoEn;
	}

	public void setCreadoEn(String creadoEn) {
		this.creadoEn = creadoEn;
	}

	public int getCargaArchivo() {
		return cargaArchivo;
	}

	public void setCargaArchivo(int cargaArchivo) {
		this.cargaArchivo = cargaArchivo;
	}

	public String getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}

	@DataField(pos = 1)
	private int opcionId;
	@DataField(pos = 2)
	private int prefijoTarjeta;
	@DataField(pos = 3)
	private String institutcionBancaria;
	@DataField(pos = 4)
	private String tipoTarjeta;
	@DataField(pos = 5)
	private String nombreBanco;
	@DataField(pos = 6)
	private int estatus;
	@DataField(pos = 7)
	private String tipoPago;
	@DataField(pos = 8)
	private int monto3Meses;
	@DataField(pos = 9)
	private int monto6Meses;
	@DataField(pos = 10)
	private int monto12Meses;
	@DataField(pos = 11)
	private String creadoEn;
	// timestamp
	@DataField(pos = 12)
	private int cargaArchivo;
	// 0 para carga por api, 1 para carga de archivo
	
	@DataField(pos = 13)
	private String exchangeId;
}
