/*
 * package com.tienda.routes;
 * 
 * import java.util.HashMap; import java.util.Map;
 * 
 * import javax.sql.DataSource;
 * 
 * import org.apache.camel.Exchange; import
 * org.apache.camel.builder.RouteBuilder; import
 * org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat; import
 * org.apache.camel.spi.DataFormat; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Component; import
 * org.springframework.stereotype.Service;
 * 
 * import com.tienda.entity.OpcionesMeses;
 * 
 * 
 * @Service public class OptionService extends RouteBuilder{
 * 
 * @Autowired DataSource datasource;
 * 
 * 
 * 
 * public void configure() throws Exception { from("direct:insert")
 * .process(this::saveSqlData) .to("jdbc:datasource");
 * 
 * 
 * }
 * 
 * private void saveSqlData(Exchange ex) throws Exception { OpcionesMeses op =
 * ex.getIn().getBody(OpcionesMeses.class); String query =
 * "INSERT INTO opcionesmeses(opcionId,prefijoTarjeta,institutcionBancaria,tipoTarjeta,nombreBanco,estatus,tipoPago,monto3Meses,monto6Meses,monto12Meses,creadoEn,"
 * + "cargaArchivo,exchangeId) values(" + op.getOpcionId() +
 * ","+op.getPrefijoTarjeta()+",'"+op.getInstitutcionBancaria()+"','"+op.
 * getTipoTarjeta()+"','"
 * +op.getNombreBanco()+"',"+op.getEstatus()+",'"+op.getTipoPago()+"',"+op.
 * getMonto3Meses()+","+op.getMonto6Meses()+","+op.getMonto12Meses()+","+op.
 * getCreadoEn()+","+op.getCargaArchivo()+","+op.getExchangeId()+")";
 * 
 * ex.getIn().setBody(query); } }
 */