package com.tienda.routes;

import com.tienda.entity.OpcionesMeses;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Component
public class FilesService extends RouteBuilder{

  DataFormat bindy = new BindyCsvDataFormat(OpcionesMeses.class);
  
  @Override
  public void configure() throws Exception {
	  

     
    from("direct:processInsert")
      .pollEnrich("file:ejercicio/input/toTransform?noop=true&fileName=prefijosMeses.csv")
        .unmarshal(bindy)
          .split(body())
          .process((ex)->{ex.getIn().setBody(buildInsert(ex, ex.getIn().getBody(OpcionesMeses.class)));})
            .to("sql: insert into integrator.opcionesmeses( opcionId,prefijoTarjeta,institutcionBancaria,tipoTarjeta,nombreBanco,estatus,tipoPago,monto3Meses,monto6Meses,monto12Meses,creadoEn,cargaArchivo,exchangeId) "
                + "values (:#opcionId, :#prefijoTarjeta, :#institutcionBancaria, :#tipoTarjeta, :#nombreBanco, :#estatus, :#tipoPago, :#monto3Meses, :#monto6Meses, :#monto12Meses, :#creadoEn, :#cargaArchivo, :#exchangeId)")
    .end();    
 }

  public Map<String,Object> buildInsert(Exchange exchange,OpcionesMeses op) throws Exception {
    Map<String,Object> opMap = new HashMap<>();
    opMap.put("opcionId", op.getOpcionId());
    opMap.put("prefijoTarjeta ", op.getPrefijoTarjeta());
    opMap.put("institutcionBancaria", op.getInstitutcionBancaria());
    opMap.put("tipoTarjeta ", op.getTipoTarjeta());
    opMap.put("nombreBanco", op.getNombreBanco());
    opMap.put("estatus",op.getEstatus());
    opMap.put("tipoPago",op.getTipoPago());
    opMap.put("monto3Meses",op.getMonto3Meses());
    opMap.put("monto6Meses",op.getMonto6Meses());
    opMap.put("monto12Meses",op.getMonto12Meses());
    opMap.put("creadoEn",LocalDate.now().toString());
    opMap.put("cargaArchivo",op.getCargaArchivo());
    opMap.put("exchangeId", exchange.getIn().getExchange().getExchangeId());
    return opMap;
  }
  
  public void buildInsert(Exchange exchange) throws Exception{
    OpcionesMeses op = exchange.getIn().getBody(OpcionesMeses.class);
    Map<String,Object> opMap = new HashMap<>();
    opMap.put("opcionId", op.getOpcionId());
    opMap.put("prefijoTarjeta ", op.getPrefijoTarjeta());
    opMap.put("institutcionBancaria", op.getInstitutcionBancaria());
    opMap.put("tipoTarjeta ", op.getTipoTarjeta());
    opMap.put("nombreBanco", op.getNombreBanco());
    opMap.put("estatus",op.getEstatus());
    opMap.put("tipoPago",op.getTipoPago());
    opMap.put("monto3Meses",op.getMonto3Meses());
    opMap.put("monto6Meses",op.getMonto6Meses());
    opMap.put("monto12Meses",op.getMonto12Meses());
    opMap.put("creadoEn",LocalDate.now().toString());
    opMap.put("cargaArchivo",op.getCargaArchivo());
    opMap.put("exchangeId", exchange.getIn().getExchange().getExchangeId());
    exchange.getIn().setBody(opMap);
}
  

 
      
}
