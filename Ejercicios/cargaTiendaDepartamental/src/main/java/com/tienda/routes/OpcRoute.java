package com.tienda.routes;

import org.apache.camel.builder.RouteBuilder;

public class OpcRoute extends RouteBuilder{

	
	  public void configure() {
		  from("timer:simple?period=10000")
		  .to("direct:processInsert")
		  .end();
	  }
}
