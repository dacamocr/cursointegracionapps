package com.curso.integracion.aplicaciones.apache.camel.routes;

import com.curso.integracion.aplicaciones.apache.camel.entity.Employees;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Component
public class FilesService extends RouteBuilder{

  DataFormat bindy = new BindyCsvDataFormat(Employees.class);
  
  @Override
  public void configure() throws Exception {
     
    /*ruta de ejemplo para mostrar el uso del pollEnrich, unmarshall y el guardado con el componente sql*/
    from("direct:processEmployee")
      .pollEnrich("file:src/main/resources/toTransform?noop=true&fileName=employeesList.csv")
        .unmarshal(bindy)
          .split(body()).process((ex)->{
             ex.getIn().setBody(buildInsert(ex, ex.getIn().getBody(Employees.class)));})
            .to("sql: insert into integrator.employees(rfc, employeeId, name, lastName, exchangeId, area , createdAt) "
                + "values (:#rfc,  :#employeeId, :#name, :#lastName, :#exchangeId, :#area , :#createdAt)")
    .end();    
 }

  public Map<String,Object> buildInsert(Exchange exchange,Employees emp) throws Exception {
    Map<String,Object> employeeMap = new HashMap<>();
      employeeMap.put("rfc", emp.getRfc());
      employeeMap.put("name", emp.getName());
      employeeMap.put("lastName", emp.getLastName());
      employeeMap.put("employeeId", emp.getEmployeeId());
      employeeMap.put("area", emp.getArea());
      employeeMap.put("exchangeId", exchange.getIn().getExchange().getExchangeId());
      employeeMap.put("createdAt", LocalDate.now().toString());
    return employeeMap;
  }
  
  public void buildInsert(Exchange exchange) throws Exception{
    Employees emp = exchange.getIn().getBody(Employees.class);
    Map<String,Object> employeeMap = new HashMap<>();
      employeeMap.put("rfc", emp.getRfc());
      employeeMap.put("name", emp.getName());
      employeeMap.put("lastName", emp.getLastName());
      employeeMap.put("employeeId", emp.getEmployeeId());
      employeeMap.put("area", emp.getArea());
      employeeMap.put("exchangeId", exchange.getIn().getExchange().getExchangeId());
      employeeMap.put("createdAt", LocalDate.now().toString());
    exchange.getIn().setBody(employeeMap);
}
  
  /*
   * ejemplos de rutas utilizando el componente core de apache camel y el componente de JPA para el llamado a beans 
   * */
  
  /**
   * rest().post("book").produces(MediaType.APPLICATION_JSON_VALUE).type(Book.class)
      .route().routeId("postBookRoute").log("--- binded ${body} ---")
      .to("jpa:" + Book.class.getName() + "?usePersist=true")
      .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201)).setBody(constant(null));
   * 
   * */
  
  
  /**
   * rest().put("book/{id}").produces(MediaType.APPLICATION_JSON_VALUE).type(Book.class).route().choice()
      .when().simple("${header.id} < 1").bean(BookRouter.class, "negativeId").otherwise()
      .process(new Processor() {
          @Override
          public void process(final Exchange exchange) {
              Book book = exchange.getIn().getBody(Book.class);
              Integer id = Integer.valueOf(exchange.getIn().getHeader("id").toString());
              book.setId(id);
              exchange.getIn().setBody(book, Book.class);
          }
      }).to("jpa:" + Book.class.getName() + "?useExecuteUpdate=true")
      .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200)).setBody(constant(null));
   */
   /**
    * rest().delete("book/{id}").produces(MediaType.APPLICATION_JSON_VALUE).route()
      .toD("jpa:com.example.home.ApacheCamelRestExample.pojo.Book"
            + "?nativeQuery=DELETE FROM library.BOOK where id = ${header.id}&useExecuteUpdate=true")
      .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON_VALUE))
      .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200)).setBody(constant(null));
    * */
  
      
}
