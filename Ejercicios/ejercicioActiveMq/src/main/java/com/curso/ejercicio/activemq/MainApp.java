package com.curso.ejercicio.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * A Camel Application
 */
@SpringBootApplication
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
	public static void main(String[] args) {
		SpringApplication.run(MainApp.class, args);
	}


}

