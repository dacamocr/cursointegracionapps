package com.curso.ejercicio.activemq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ActiveMqSenderRouter extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		
		//from("timer:active-mq-timer?timer=10000")
		from("timer:active-mq-timer?timer=10000")
		.transform().constant("Enviar este msj desde app 1")
		.log("${body}")
		.to("activemq:my-activemq-queue");
	}

}
