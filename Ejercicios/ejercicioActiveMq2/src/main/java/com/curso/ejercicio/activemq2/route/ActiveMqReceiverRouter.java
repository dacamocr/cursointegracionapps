package com.curso.ejercicio.activemq2.route;



import org.apache.camel.builder.RouteBuilder;

import org.springframework.stereotype.Component;

@Component
public class ActiveMqReceiverRouter extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		from("activemq:my-activemq-queue")
		.transform().constant("este recibe")
//		.log("${body}")
		.to("log:received-message-from-active-mq");

	}


}
