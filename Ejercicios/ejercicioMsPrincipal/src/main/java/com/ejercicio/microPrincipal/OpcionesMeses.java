package com.ejercicio.microPrincipal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "opcionesmeses")
public class OpcionesMeses {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getOpcionId() {
		return opcionId;
	}

	public void setOpcionId(int opcionId) {
		this.opcionId = opcionId;
	}

	public int getPrefijoTarjeta() {
		return prefijoTarjeta;
	}

	public void setPrefijoTarjeta(int prefijoTarjeta) {
		this.prefijoTarjeta = prefijoTarjeta;
	}

	public String getInstitutcionBancaria() {
		return institutcionBancaria;
	}

	public void setInstitutcionBancaria(String institutcionBancaria) {
		this.institutcionBancaria = institutcionBancaria;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public int getMonto3Meses() {
		return monto3Meses;
	}

	public void setMonto3Meses(int monto3Meses) {
		this.monto3Meses = monto3Meses;
	}

	public int getMonto6Meses() {
		return monto6Meses;
	}

	public void setMonto6Meses(int monto6Meses) {
		this.monto6Meses = monto6Meses;
	}

	public int getMonto12Meses() {
		return monto12Meses;
	}

	public void setMonto12Meses(int monto12Meses) {
		this.monto12Meses = monto12Meses;
	}

	public String getCreadoEn() {
		return creadoEn;
	}

	public void setCreadoEn(String creadoEn) {
		this.creadoEn = creadoEn;
	}

	public int getCargaArchivo() {
		return cargaArchivo;
	}

	public void setCargaArchivo(int cargaArchivo) {
		this.cargaArchivo = cargaArchivo;
	}

	public String getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}

	private int opcionId;
	private int prefijoTarjeta;
	private String institutcionBancaria;
	private String tipoTarjeta;
	private String nombreBanco;
	private int estatus;
	private String tipoPago;
	private int monto3Meses;
	private int monto6Meses;
	private int monto12Meses;
	private String creadoEn;
	private int cargaArchivo;
	private String exchangeId;
	@Override
	public String toString() {
		return "OpcionesMeses [opcionId=" + opcionId + ", prefijoTarjeta=" + prefijoTarjeta + ", institutcionBancaria="
				+ institutcionBancaria + ", tipoTarjeta=" + tipoTarjeta + ", nombreBanco=" + nombreBanco + ", estatus="
				+ estatus + ", tipoPago=" + tipoPago + ", monto3Meses=" + monto3Meses + ", monto6Meses=" + monto6Meses
				+ ", monto12Meses=" + monto12Meses + ", creadoEn=" + creadoEn + ", cargaArchivo=" + cargaArchivo
				+ ", exchangeId=" + exchangeId + "]";
	}
}
