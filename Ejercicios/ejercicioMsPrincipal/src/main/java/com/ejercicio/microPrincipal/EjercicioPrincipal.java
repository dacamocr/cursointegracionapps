package com.ejercicio.microPrincipal;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/**
 * A Camel Java DSL Router
 */
@Component
public class EjercicioPrincipal extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		 restConfiguration()
		    .contextPath("/rest/*")
		    .apiContextPath("/api-doc")
		    .apiProperty("api.title", "Spring Boot Camel Postgres Rest API.")
		    .apiProperty("api.version", "1.0")
		    .apiProperty("cors", "true")
		    .apiContextRouteId("doc-api")
		    .port("8090")
		    .bindingMode(RestBindingMode.json);
		    
		    rest("/book")
		    .consumes(MediaType.APPLICATION_JSON_VALUE)
		    .produces(MediaType.APPLICATION_JSON_VALUE)
		    .get("/{name}").route()
		    .to("direct:findAllBooks")
		    .endRest()
		    ;
		    
		    from("direct:findAllBooks")
		    .log("ok");
		  }
	
	



}
