package com.ejercicio.microPrincipal;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.apache.camel.component.jackson.JacksonDataFormat;

@Component
public class EjercicioRoute extends RouteBuilder{

	    private final Environment env;

	    public EjercicioRoute(Environment env) {
	        this.env = env;
	    }

	    public void configure() throws Exception {

	        restConfiguration()
	                .contextPath(env.getProperty("camel.component.servlet.mapping.contextPath", "/rest/*"))
	                .apiContextPath("/api-doc")
	                .apiProperty("api.version", "1.0")
	                .apiProperty("cors", "true")
	                .apiContextRouteId("doc-api")
	                .port(env.getProperty("server.port", "8080"))
	                .bindingMode(RestBindingMode.json);

	        rest("/opcion")
	                .consumes(MediaType.APPLICATION_JSON_VALUE)
	                .produces(MediaType.APPLICATION_JSON_VALUE)
	                .get("/{name}").route()
	                .to("{{route.findByOpcionId}}")
	                .endRest()
	                .get("/").route()
	                .to("{{route.findAllOpciones}}")
	                .endRest()
	                .post("/").route()
	                .marshal().json()
	                .unmarshal(getJacksonDataFormat(OpcionesMeses.class))
	                .to("{{route.saveOpcion}}")
	                .endRest()
	                .delete("/{opciond}").route()
	                .to("{{route.removeOpcion}}")
	                .end();

	        from("{{route.findByOpcionId}}")
	                .bean(EjercicioService.class, "findBookByName(${header.name})");

	        from("{{route.findAllOpciones}}")
	                .bean(EjercicioService.class, "findAllOpciones");


	        from("{{route.saveOpcion}}")
	                .bean(EjercicioService.class, "addOpcion(${body})");


	        from("{{route.removeOpcion}}")
	                .bean(EjercicioService.class, "removeOpcion(${header.opcionId})");
	    }

	    private JacksonDataFormat getJacksonDataFormat(Class<?> unmarshalType) {
	        JacksonDataFormat format = new JacksonDataFormat();
	        format.setUnmarshalType(unmarshalType);
	        return format;
	    }

}
