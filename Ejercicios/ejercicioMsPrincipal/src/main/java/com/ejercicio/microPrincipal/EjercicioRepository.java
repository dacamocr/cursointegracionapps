package com.ejercicio.microPrincipal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EjercicioRepository extends JpaRepository<OpcionesMeses, Integer> {

    OpcionesMeses findOpionByOpcionId(String name);
}
