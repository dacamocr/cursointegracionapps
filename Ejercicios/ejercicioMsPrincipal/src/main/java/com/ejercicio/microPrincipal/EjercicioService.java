package com.ejercicio.microPrincipal;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class EjercicioService {

    private final EjercicioRepository ejercicioRepository;

    public EjercicioService(EjercicioRepository ejercicioRepository) {
        this.ejercicioRepository = ejercicioRepository;
    }

    public List<OpcionesMeses> findAllOptions() {
        return ejercicioRepository.findAll();
    }

    public OpcionesMeses findByOpcionId(String opcion) {
        return ejercicioRepository.findOpionByOpcionId(opcion);
    }

    public OpcionesMeses addOpcion(OpcionesMeses opcion) {
        return ejercicioRepository.save(opcion);
    }

    public void removeOpcion(int opcionId) {
    	ejercicioRepository.deleteById(opcionId);
    }
    
}