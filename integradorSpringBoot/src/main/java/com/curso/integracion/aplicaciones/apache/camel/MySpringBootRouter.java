package com.curso.integracion.aplicaciones.apache.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * A simple Camel route that triggers from a timer and calls a bean and prints to system out.
 * <p/>
 * Use <tt>@Component</tt> to make Camel auto detect this route when starting.
 */


public class MySpringBootRouter extends RouteBuilder {

    @Override
    public void configure() {
      /*ruta de ejemplo donde se ejecuta el llamado a un bean configurado por el arqutipo, utiliza los componentes de timer y stream*/
        from("timer:hello?period={{timer.period}}").routeId("hello")
            .transform().method("myBean", "saySomething")
            .filter(simple("${body} contains 'foo'"))
                .to("log:foo")
            .end()
            .to("stream:out");
        
      
      /*ruta donde se utiliza la funcionalidad de loadBalance con una opcion de random, con el fin de realizar un llamado a una serie de 
       * rutas de ejemplo*/
        from("timer:hello?period={{timer.period}}")
        .loadBalance().random()
        .to("direct:load1")
        .to("direct:load2")
        .to("direct:load3")
        .to("direct:load4")
        .to("direct:load5");
        
        /*rutas de ejemplo las cuales son llamadas por el algoritmo balanceador en ese caso es random*/
        from("direct:load1").log("load1").delay(3000);
        from("direct:load2").log("load2").delay(4000);
        from("direct:load3").log("load3").delay(1000);
        from("direct:load4").log("load4").delay(3000);
        from("direct:load5").log("load5").delay(3500);
    }

}
